from flask import render_template, current_app as app

import psutil
import platform
import datetime


@app.route("/")
def index():
    return render_template("index.html")

@app.route("/monitor")
def monitor():
    return render_template("monitor.html")
